public class Board{
	private Tile[][] grid;
	
	public Board() {
		grid = new Tile[3][3];
		for (int i = 0; i<grid.length; i++) {
			for (int j = 0; j<grid[i].length; j++) {
				grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	public String toString(){
		String output = "";
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid.length; j++) {
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	public void placeToken(int x, int y, String playerToken) {
		if (this.grid[y][x].equals(Tile.BLANK)) {
			if (playerToken.equals("X")) {
				this.grid[y][x] = Tile.X;
			}
			
			if (playerToken.equals("O")) {
				this.grid[y][x] = Tile.O;
			}
		}
		else {
			System.out.println("ERROR");
		}
	}
}